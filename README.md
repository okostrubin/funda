# DevOps Assignment

## Requirements

- Docker
- Run `docker swarm init` or `docker swarm join` command to initialize a swarm

## Deployment of stack

Add AWS credentials to `.env` file.

Run:
`./deploy.sh`

Check status of deployment using `docker stack services funda` command.

## Prometheus

Here you can see alerting rules:

`http://localhost:9090/alerts`

Edit `prometheus-funda/rules.yml` for rules modification.

## Alertmanager

Here you can see how alerts are handled:

`http://localhost:9093/#/alerts`

## Removing stack

Run:
`./remove.sh`

## Technologies

- Docker
- Shell
- Prometheus
- Alertmanager
- Amazon Simple Queue Service (SQS) Exporter