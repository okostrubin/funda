#!/bin/sh

set -e

docker build -t prometheus-funda:1.0.0 ./prometheus-funda
docker build -t alertmanager-funda:1.0.0 ./alertmanager-funda
docker build -t sqs-exporter-funda:1.0.0 ./sqs-exporter-funda

env $(cat .env | grep ^[A-Z] | xargs) docker stack deploy -c funda.yml funda